from enum import IntEnum


class {{ obj.name }}(IntEnum):
    """{{ obj.summary }}"""
    __tdx_type__ = '{{ obj.tdx_type }}'
{% for prop in obj.properties %}
    {% for line in prop.summary_lines -%}
    #: {{ line }}
    {% endfor -%}
    {{ prop.snake_name.upper() }} = {{ prop.type_value }}
{% endfor %}