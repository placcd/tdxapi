import time

import pytest

import tools
from tools import scraper

enum_classes = tools.get_enum_classes()


@pytest.mark.parametrize("enum", enum_classes)
def test_enum_attributes(enum):
    # throttle hits to web api docs
    time.sleep(1)

    api_obj = scraper.get_api_object(enum.__tdx_type__)

    api_properties = [p.snake_name.upper() for p in api_obj.properties]
    existing_properties = [e for e in enum.__members__.keys()]

    assert sorted(api_properties) == sorted(existing_properties)
