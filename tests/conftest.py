import pytest

from tdxapi import TdxClient


def pytest_addoption(parser):
    parser.addoption("--org", action="store", default=None)
    parser.addoption("--beid", action="store", default=None)
    parser.addoption("--wskey", action="store", default=None)


@pytest.fixture()
def org(request):
    return request.config.getoption("--org")


@pytest.fixture()
def beid(request):
    return request.config.getoption("--beid")


@pytest.fixture()
def wskey(request):
    return request.config.getoption("--wskey")


@pytest.fixture()
def tdx(org, beid, wskey, use_sandbox=True):
    if not all([org, beid, wskey]):
        raise ValueError("Required pytest options: --org, --beid, and --wskey")

    return TdxClient(org, beid, wskey, use_sandbox)
